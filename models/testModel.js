var sql= require('../db');

var testModel={

};
testModel.getAllTest=function(result){
    sql.query("SELECT * FROM locker",function(err,res){
        if(err) {
            return result(err,null);
        }
        else{
            return result(null,res);
        }
    });
}
testModel.insertTest=function(newLocker,result)
{
    sql.query("INSERT INTO locker SET ?",newLocker,function(err,res){
        if(err){
            return result(err,null);
        }else{
            return result(null,res);
        }
    });
}
testModel.findTestById=function(lockerId,result){
    sql.query("SELECT * FROM locker WHERE id ="+lockerId,function(err,rows){
        if(err)
            throw err;

        if (rows.length <= 0) {
            return result(err);
        }
        else {
            return result(rows);
        }
    })
}

testModel.updateTest=function(lockerId,locker,result){
    sql.query("UPDATE locker SET  ? WHERE id="+lockerId,locker,function(err,rows){
        if(err)
            result(err);

        return result(rows);

    });
}

testModel.generateLockerStatus=function(lockerStatusId){
    sql.query("SELECT name FROM locker_status WHERE id="+lockerStatusId,lockerStatus,function(err,rows){
        if(err)
            result(err);

        return result(rows);

    });
};
module.exports=testModel;