/***
 * *     Website
 ***/
const express = require('express');
const fs = require('fs');
const app = express();
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const cookieParser=require('cookie-parser');
const session= require('express-session');
const flash = require('express-flash');
const routes = require('./routes/routes');
const path = require('path');
const methodOverride = require('method-override');
const moment= require('moment');

const eServerCOM = {
    COM1: 1,
    COM2: 2
};



let dataJSON = [
    {
        lockerNumber: 1,
        doorStatus: 0,
        deviceStatus: 0,
        currentStatus: 0
    },
    {
        lockerNumber: 2,
        doorStatus: 0,
        deviceStatus: 0,
        currentStatus: 0
    },
    {
        lockerNumber: 3,
        doorStatus: 0,
        deviceStatus: 0,
        currentStatus: 0
    },
    {
        lockerNumber: 4,
        doorStatus: 0,
        deviceStatus: 0,
        currentStatus: 0
    }
];

/***
 * *     config.json
 ***/
const configPath = fs.readFileSync('config.json', 'utf-8', function(err, data) {
    if(err) {
        return console.log(err);
    }
    return data;
});

const config = JSON.parse(configPath);
const port = config.port;
const com = config.serialport;
const baudRate = config.baudRate;
console.log('===========================');
console.log('========SETUP START========');
console.log("Port: " + port)
console.log("BaudRate: " + baudRate)

const SerialPort = require("serialport");
const serial_COM_1 = SerialPort("COM1", baudRate);
console.log("[Connect] Serialport: " + serial_COM_1.path)
const serial_COM_2 = SerialPort("COM2", baudRate);
console.log("[Connect] Serialport: " + serial_COM_2.path)

app.locals.moment=moment;
app.locals.shortDateFormat="MM/DD/YYYY";
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use('/static', express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(cookieParser('keyboard cat'));
app.use(session({ 
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}));

app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      let method = req.body._method;
      delete req.body._method;
      return method
    }
  }));
app.use(flash());
app.use(bodyParser.json());
app.use('/', routes);


// handle AJAX from client
app.use('/control', express.json());
app.use('/getData', express.json());

app.post('/control', function (req, res) {
    writeToSerial(req.body.com, req.body.id, req.body.value);
    res.send(req.body);
});

app.get("/getData", function (req, res) {
    res.send(dataJSON);
    // refreshTest();
});

app.listen(port, function () {
    console.log("App was started");
});


/***
 * *     Serial Port Connection
 ***/
// Test the connection and raise error if something went wrong
serial_COM_1.write('0', function (err) {
    if (err) {
        return console.log('Error on write: ', err.message);
    }
    console.log('[TEST] write on: ' + serial_COM_1.path);
});

// Test the connection and raise error if something went wrong
serial_COM_1.write('0', function (err) {
    if (err) {
        return console.log('Error on write: ', err.message);
    }
    console.log('[TEST] write on: ' + serial_COM_2.path);
    console.log('=========SETUP END=========');
    console.log('===========================');
});


/***
 * *     write to Serial Port
 ***/
function writeToSerial(comPort, id, value) {

    let comport;
    let cmd;

    switch(comPort){
        case eServerCOM.COM1:
            comport = serial_COM_1;
            cmd = Buffer.from([0x90, 0x05, id, value, 0x03])
            break;

        case eServerCOM.COM2:
            comport = serial_COM_2;
            cmd = Buffer.from([0x90, 0x05, id, 0x03, value])
            break;
        default:
            break;
    }

    comport.write(cmd, function(err) {
        if (err) {
            return console.log('Error on write: ', err.message);
        }
        console.log('[WRITE] => BufferStream (' + comport.path + '): ' + cmd.toString('hex').replace(/(.)(.)/g, '$1$2 ') + ' => successful');
    });

}


/***
 * *     read from Serial Port
 ***/
serial_COM_1.on("open", function () {
    console.log('[TEST] read from: ' + serial_COM_1.path);
    serial_COM_1.on('data', function(data) {
        console.log('[READ] => BufferStream (COM1): ' + data.toString('hex').replace(/(.)(.)/g, '$1$2 '));

        console.log("Bufferlänge: " + data.length);
        if (data.length > 5){
            if(data.length % 5 !== 0){
                console.log("[ERROR] - The inputBuffer was not correct");
                return 0;
            }

            for(let i = 0; i < (data.length/5); i++){
                let bufferSlice = data.slice(i * 5, i * 5 + 5);
                console.log("BufferSlice: ");
                console.log(bufferSlice);
                updateLockerData(bufferSlice);
            }

        }else{
            updateLockerData(data);
        }

    });
});

serial_COM_2.on("open", function () {
    console.log('[TEST] read from: ' + serial_COM_2.path);
    serial_COM_2.on('data', function(data) {
        console.log('[READ] => BufferStream (COM2): ' + data.toString('hex').replace(/(.)(.)/g, '$1$2 '));
        updateCoinSlotData(data);
    });
});

function updateLockerData(buffer){
    let bufferJSON = buffer.toJSON();

    let buffer_id = bufferJSON.data[2];
    let buffer_value =bufferJSON.data[3];

    // console.log("ID: " + buffer_id);
    // console.log("VALUE: " + buffer_value);

    if(buffer_id >= 193 && buffer_id <= 200){
        buffer_id = buffer_id - 192;
        console.log("setDeviceStatus: Fach: " + buffer_id + " Status: " + buffer_value);
        dataJSON[buffer_id - 1].deviceStatus = buffer_value;
        // setDeviceStatus(buffer_id, buffer_value);

    } else if(buffer_id >= 177 && buffer_id <= 184){
        buffer_id = buffer_id - 176;
        console.log("setCurrentStatus: Fach " + buffer_id + " Status: " + buffer_value);
        dataJSON[buffer_id - 1].currentStatus = buffer_value;
        // setCurrentStatus(buffer_id, buffer_value);

    } else if(buffer_id >= 161 && buffer_id <= 168){
        buffer_id = buffer_id - 160;
        console.log("setDoorStatus: Fach " + buffer_id + " Status: " + buffer_value);
        dataJSON[buffer_id - 1].doorStatus = buffer_value;
        // setDoorStatus(buffer_id, buffer_value);

    }

}

function updateCoinSlotData(buffer){
    let bufferJSON = buffer.toJSON();

    let buffer_id = bufferJSON.data[2];
    let buffer_value =bufferJSON.data[3];

    // console.log("ID: " + buffer_id);
    // console.log("VALUE: " + buffer_value);

    // console.log("Coinsteuerung")
    console.log("Coin: " + buffer_value);
    // setMoney(buffer_value);

}



