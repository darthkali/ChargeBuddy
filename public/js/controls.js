function sendControlData(com, id, value) {

    const dataObject = { com: com, id: id, value: value};
    const dataString = JSON.stringify(dataObject);
    const dynamicTransactions = new XMLHttpRequest();
    const URL = "/control";
    dynamicTransactions.open("POST", URL, true);
    dynamicTransactions.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    dynamicTransactions.send(dataString);
}
//
setInterval(refreshTest, 50); //TODO: set to 50 or less
//
function refreshTest() {
    const dynamicTransactions = new XMLHttpRequest();
    const URL = "/getData";
    dynamicTransactions.onreadystatechange = () => {
        if (dynamicTransactions.readyState === 4 && dynamicTransactions.status === 200) {
            const parsed = JSON.parse(dynamicTransactions.responseText);
            for(let i = 0; i < 4 ; i++){
                document.getElementById('lockerNumber' + (i + 1)).innerText = parsed[i].lockerNumber;
                document.getElementById('doorStatus'+ (i + 1)).innerText = parsed[i].doorStatus;
                document.getElementById('deviceStatus'+ (i + 1)).innerText = parsed[i].deviceStatus;
                document.getElementById('currentStatus'+ (i + 1)).innerText = parsed[i].currentStatus;
            }
        }
    };
    dynamicTransactions.open("GET", URL, true);
    dynamicTransactions.setRequestHeader('Cache-Control', 'no-cache');
    dynamicTransactions.send();
}

function update(newValue) {
    document.getElementById(this.position + '.4').innerText = newValue;
}
