let testModel= require('../models/testModel');
let testController=function(){};

testController.index=function(req,res){
    testModel.getAllTest(function(err,lockers) {

        if(err){
            throw err;
        }else{
            res.render('test/index', {title: 'Home page', lockers: lockers});
        }

    });
};

module.exports=testController;
